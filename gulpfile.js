'use strict';

const gulp = require('gulp');
const imagemin = require('gulp-imagemin');
const smushit = require('gulp-smushit');
const imageminJpegtran = require('imagemin-jpegtran');
const image = require('gulp-image');

// Lossy
gulp.task('image', function () {
  gulp.src('img-src/**/*')
    .pipe(image({
      pngquant: true,
      optipng: false,
      zopflipng: true,
      jpegRecompress: false,
      jpegoptim: true,
      mozjpeg: true,
      gifsicle: false,
      svgo: false,
      concurrent: 10
    }))
    .pipe(gulp.dest('img'))
});

// Lossless
gulp.task('imagemin', () =>
  gulp.src('img-src/**/*')
    .pipe(imagemin(
      [imageminJpegtran({progressive: true, buffer: 1})],
      {verbose: true,}
    ))
    .pipe(gulp.dest('img'))
);

gulp.task('smushit', function () {
  gulp.src('img-src/**/*')
        .pipe(smushit({
            verbose: true
        }))
        .pipe(gulp.dest('img'));
});

gulp.task('default', ['imagemin']);
