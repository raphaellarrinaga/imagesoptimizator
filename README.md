# Images Optimizator

Images Optimizator is just a gulp configuration for image optimisation.

## Installation  ##

### Clone this repository

### Make sure Grunt CLI is installed globally ###

You may need to use sudo (for OSX, *nix, BSD etc) or run your command shell as Administrator (for Windows) to do this.

    npm install -g grunt-cli

### Install the necessary grunt modules ###

In the root folder of that project aka the same folder as `gulpfile.js`

    npm install

## Usage ##

- create 2 folders in the root folder: `img-src`, `img`
- put your files in the `img-src` folder.
- use `gulp` to compress with `gulp-imagemin`
- use `gulp smushit` to compress with `gulp-smushit` (compression rates are differents)
- use `gulp smushit` to compress with `gulp-image` (lossy compression)
- get your optimized images in the `img` folder
